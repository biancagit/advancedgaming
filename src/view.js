lib = window.lib || {};

(function () {
    class SnakeView {

        /**
         * Constructor for the SnakeView.
         *
         * this.fieldSize   {number}    pixel-size of one box
         * this._model      {object}    the corresponding model
         * this._dom        {}          shortcut to the most important DOM-elements
         *
         * @author  Lisa Koller, Bianca Höllmüller, Maria Sams
         * @param   model        the brain of the game
         */
        constructor(model) {
            //20 Pixels is the SVG standard width and height for our View
            this.fieldSize = 20;
            this._model = model;
            this._dom = {
                $field: $(".field"),
                $score: $(".scoreinput")
            };

            // Event bindings
            $(model).on('initGame', this._init.bind(this));
            $(model).on('gameOver', this._gameOver.bind(this));
            $(model).on('moveSnake', this._render.bind(this));
            $(model).on('caughtItem', this._makeSnakeLonger.bind(this));

            //init keydowns and button click
            this._initHandlers();
        }

        //------------ private ------------//

        /**
         * Initializes listeners on the WSAD and arrow-keys
         * to enable users to change the direction of the snake.
         * Calls the initializer of the model when the button to start
         * the game is clicked.
         * Makes sure that the user cannot change the direction in the
         * exact opposite direction of the current direction which would
         * result in an instant game over (as it hits itself).
         *
         * @author Lisa Koller
         * @private
         */
        _initHandlers() {
            $(document).keydown((e) => {
                let keycode = e.which;

                switch(keycode) {
                    case 37: // left
                    case 65:
                        //disable scrolling with arrow keys
                        e.preventDefault();
                        if(this._model.curDirection !== "right") this._model.setCurDirection("left");
                        break;
                    case 38: // up
                    case 87:
                        //disable scrolling with arrow keys
                        e.preventDefault();
                        if(this._model.curDirection !== "down") this._model.setCurDirection("up");
                        break;
                    case 39: // right
                    case 68:
                        //disable scrolling with arrow keys
                        e.preventDefault();
                        if(this._model.curDirection !== "left") this._model.setCurDirection("right");
                        break;
                    case 40: // down
                    case 83:
                        //disable scrolling with arrow keys
                        e.preventDefault();
                        if(this._model.curDirection !== "up") this._model.setCurDirection("down");
                        break;
                }
            });

            $(".snakeBtn").click(() => { this._model.init() } );
        }

        /**
         * Creates the html-elements of the snake when the game starts and
         * appends them to the snakeContainer-div. After that it calls the
         * render function of the view.
         *
         * @author Lisa Koller
         * @param  e    event-information
         * @private
         */
        _init(e){
            //remove possible old content of container
            $(".snakeContainer").empty();

            //head
            let snake = '<img class="snakeSVG" id="snake_head" src="./img/snake_head.svg"/>';

            //body
            for(let i = 1; i < e.snakeArr.length-1; i++){
                snake += '<img class="snakeSVG" id="snake_body_' + i + '" src="./img/snake_body.svg"/>';
            }

            //tail
            snake += '<img class="snakeSVG" id="snake_tail" src="./img/snake_tail.svg"/>';

            $('.snakeContainer').append(snake);

            //render the snake
            this._render(e);
        }

        /**
         * Renders items and snake.
         *
         * @param e event-information
         * @author Bianca Höllmüller, Lisa Koller
         * @private
         */
        _render(e) {
            this._drawItems();
            this._drawSnake(e);
            this._dom.$score.html(e.score);
        }

        /**
         * Diamond-Item will be set in the right place in the field.
         *
         * @author Maria Sams
         * @private
         */
        _drawItems() {
            let html = '<img class="itemSVG" id="diamond" src="./img/diamond.svg"/>';
            $('.diamondContainer').empty().append(html);
            $("#diamond").css({
                'left': (this._model._itemCoords.x*20)+'px',
                'top': (this._model._itemCoords.y*20)+'px'
            });
        }

        /**
         * Snake-Parts Head, Body and Tail will be set in the right place in the field
         * and rotates
         *
         * @author Maria Sams
         * @private
         */
        _drawSnake() {
            //Snake Head rotation
            switch(this._model.getHeadDirection()) {
                case "right":
                    $("#snake_head").css("transform","rotate(90deg)");
                    break;
                case "down":
                    $("#snake_head").css("transform","rotate(180deg)");
                    break;
                case "left":
                    $("#snake_head").css("transform","rotate(270deg)");
                    break;
                case "up":
                    $("#snake_head").css("transform","rotate(0deg)");
                    break;
            }

            //Snake Body Position
            for(let i = 1; i <= this._model._snake.length-2; i++){
                //sets every body-part with the bodyID to the right position
                $('#snake_body_' + i).css({
                    'left': (this._model._snake[i].x*this.fieldSize)+'px',
                    'top': (this._model._snake[i].y*this.fieldSize)+'px'
                });
            }

            //Snake Head Position
            $("#snake_head").css({
                'left': (this._model._snake[0].x*this.fieldSize)+'px',
                'top': (this._model._snake[0].y*this.fieldSize)+'px'
            });

            //Snake Tail rotation
            switch(this._model.getTailDirection()) {
                case "right":
                    $("#snake_tail").css("transform","rotate(90deg)");
                    break;
                case "down":
                    $("#snake_tail").css("transform","rotate(180deg)");
                    break;
                case "left":
                    $("#snake_tail").css("transform","rotate(270deg)");
                    break;
                case "up":
                    $("#snake_tail").css("transform","rotate(0deg)");
                    break;
            }

            //Snake Tail Position
            $("#snake_tail").css({
                'left': (this._model._snake[this._model._snake.length-1].x*this.fieldSize)+'px',
                'top': (this._model._snake[this._model._snake.length-1].y*this.fieldSize)+'px'
            });
        }

        /**
         * Creates a new body piece of the snake and appends it to the container
         * (used in case the game gets more difficult thus the snake longer).
         *
         * @author Lisa Koller
         * @private
         */
        _makeSnakeLonger(){
            let snakeArr = this._model._snake;
            let i = snakeArr.length-1;

            //create html-element and append it to the DOM
            let snake = '<img class="snakeSVG" id="snake_body_' + i + '" src="./img/snake_body.svg"/>';
            $('.snakeContainer').append(snake);
        }

        /**
         * game over screen
         *
         * @author Lisa Prammer
         * @private
         */
        _gameOver() {
            $("body").append("<div class='gameover'><p>Game Over!</p></div>");
            $(".snakeBtn").click(() => { window.location.reload(true); });
            $("body").append("<div class='gameover'><p>Game Over!</p><p class='bottom'>Score: "+this._model._score+"</p></div>");
            $(document).off('keydown');
        }
    }

    lib.SnakeView = SnakeView;

}());