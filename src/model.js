lib = window.lib || {};

(function () {
    class SnakeModel {

        /**
         * Constructor for the SnakeModel.
         *
         * this._score          {number}    points the player achieved
         * this._itemCoords     {}          coordinates of the item to catch
         * this._fieldCoords    {}          min/max and start of field
         * this._snake          []          array which contains the snake parts
         * this._curDirection   {string}    direction the snake is currently moving towards
         * this._startTime      {number}    Timer Seconds
         * this._minuteTime     {number}    Timer Minutes
         *
         * @author Lisa Koller, Lisa Prammer, Bianca Höllmüller
         *
         */
        constructor() {
            this._score = 0;
            this._itemCoords = { x: 5, y: 6};
            //in der View max 500 -> PixelFeld 20 -> 500/20 = 25 -> model max also 25
            this._fieldCoords = { minX: 0, minY: 0, maxX: 25, maxY: 25};
            this._snake = [];
            this.curDirection = "right";
            this._startTime = 0;
            this._minuteTime = 0;
        }

        //------------ public ------------//

        /**
         * Initializes the game by filling the snake array with initial
         * numbers, resetting the score and resetting the direction.
         * Additionally the "initGame" event is triggered and
         * the interval for moving the snake set.
         *
         * @author Bianca Höllmüller, Lisa Koller
         */
        init() {
            this._snake = [{x: 2, y: 10},{x: 1, y: 10},{x: 0, y: 10}];
            this._resetScore();
            this._resetDirection();

            //events
            const e = new $.Event("initGame", {
                direction: "right",
                snakeArr: this._snake,
                score: this._score
            });
            $(this).trigger(e);

            //call moveSnake every few milliseconds
            let interval = setInterval(this.moveSnake.bind(this), 250);
            $(this).on('gameOver', function(){
                //remove interval on game over
                clearInterval(interval);
            });
            $(this).on('caughtItem', function(){
                //remove interval when item is caught, because a new (faster) one is created
                clearInterval(interval);

                //calculate a new, higher speed based on the current score
                let speed = 250-(this._score*0.75);
                //set a limit for the speed
                if(speed < 80) speed = 80;

                interval = setInterval(this.moveSnake.bind(this), speed);
            })
        }

        /**
         * prints time every second
         *
         * @author Lisa Prammer
         */
        start() {
            let interval = setInterval(()=>{

                if(this._startTime == 60){
                    this._minuteTime++;
                    this._startTime=0;
                    $('.timeinput').html(this._minuteTime+':0'+this._startTime);
                }
                if(this._startTime<10) {
                    $('.timeinput').html(this._minuteTime + ':0' + this._startTime);
                } else {
                    $('.timeinput').html(this._minuteTime + ':' + this._startTime);
                }
                this._startTime++
            }, 1000);

            $(this).on('gameOver', function(){clearInterval(interval)});
        };

        /**
         * Sets the current direction the snake is moving towards.
         *
         * @return {boolean}    - true if operation was successful
         *                      - false if operation was not successful
         * @author Lisa Koller
         * @param  direction     {string}     - "right" || "down" || "left" || "up"
         */
        setCurDirection(direction){
            //only set direction if viable direction
            if(direction === "right" || direction === "down" || direction === "left" || direction === "up"){
                this.curDirection = direction;
                return true;
            }
            else return false;
        }

        /**
         * Moves the snake in the specified direction by changing
         * the coordinates of the array entries. Also checks if an
         * collision occurred or an item was caught. Finally
         * trigger the "moveSnake"-event to let the view know
         * to render the snake.
         *
         * @author Lisa Koller
         */
        moveSnake(){
            //body
            for(let i = this._snake.length-1; i > 0; i--){
                this._snake[i].x = this._snake[i-1].x;
                this._snake[i].y = this._snake[i-1].y;
            }

            //head
            if(this.curDirection === "up"){
                this._snake[0].y --;
            }
            if(this.curDirection === "down"){
                this._snake[0].y ++;
            }
            if(this.curDirection === "left"){
                this._snake[0].x --;
            }
            if(this.curDirection === "right"){
                this._snake[0].x ++;
            }

            //check for collision
            if(this._checkCollision()) {
                //trigger event "gameOver" and return
                $(this).trigger("gameOver");
                return;
            }

            //check if caught an item
            if(this._hasItItem()){
                //increase the score and make snake longer 😈
                this._score += 10;
                this._makeSnakeLonger();

                //place a new item
                this._setRandomItemCoords();
            }

            //trigger moveSnake event
            const e = new $.Event("moveSnake", {
                direction: this.curDirection,
                snakeArr: this._snake,
                score: this._score
            });
            $(this).triggerHandler(e); //e
        }

        /**
         * Returns the direction the head of the snake is moving towards.
         *
         * @return  {string}   direction    - "right" || "down" || "left" || "up"
         * @author  Lisa Koller
         */
        getHeadDirection(){
            return this.curDirection;
        }

        /**
         * Returns the direction the tail of the snake is moving towards.
         *
         * @return  {string}   direction    - "right" || "down" || "left" || "up" || null (in case nothing matched)
         * @author  Lisa Koller
         */
        getTailDirection(){
            //get needed coords
            let length = this._snake.length;
            let tailX = this._snake[length-1].x;
            let tailY = this._snake[length-1].y;
            let bodyX = this._snake[length-2].x;
            let bodyY = this._snake[length-2].y;

            //compare coords to get the direction of the last part of the snake
            if(tailX === bodyX && tailY < bodyY){
                return "down";
            }
            if(tailX === bodyX && tailY > bodyY){
                return "up";
            }
            if(tailX < bodyX && tailY === bodyY){
                return "right";
            }
            if(tailX > bodyX && tailY === bodyY){
                return "left";
            }
            else{
                return null;
            }
        }

        //------------ private ------------//

        /**
         * Resets the score to zero.
         *
         * @author Lisa Koller
         * @private
         */
        _resetScore(){
            this._score = 0;
        }

        /**
         * Resets the current direction to "right".
         *
         * @author Lisa Koller
         * @private
         */
        _resetDirection(){
            this.curDirection = "right";
        }

        /**
         * Checks if snake has hit wall or itself.
         *
         * @returns {boolean}
         * @author Bianca Höllmüller
         * @private
         */
        _checkCollision() {
            return this._hasHitWall() || this._hasHitItself();
        }

        /**
         * Checks if snake has hit wall.
         *
         * @returns {boolean}
         * @author Bianca Höllmüller, Maria Sams
         * @private
         */
        _hasHitWall() {
            let head = this._snake[0];
            return head.x === this._fieldCoords.minX-1 || head.x === this._fieldCoords.maxX ||
                head.y === this._fieldCoords.minY-1 || head.y === this._fieldCoords.maxY;
        }

        /**
         * Checks if snake has hit itself.
         *
         * @returns {boolean}
         * @author Bianca Höllmüller
         * @private
         */
        _hasHitItself() {
            let head = this._snake[0];
            for(let i = 1; i < this._snake.length; i++)
                if(head.x === this._snake[i].x && head.y === this._snake[i].y) return true;
            return false;
        }

        /**
         * Checks if snake has catches an item.
         *
         * @returns {boolean}   - true if it caught an item
         *                      - false if no item was caught
         * @author  Lisa Koller
         * @private
         */
        _hasItItem() {
            let head = this._snake[0];
            if(head.x === this._itemCoords.x && head.y === this._itemCoords.y){
                //create and trigger the event
                const e = new $.Event("caughtItem", {
                    item: this._itemCoords
                });
                $(this).triggerHandler(e);
                return true;
            }
            else return false;
        }

        /**
         * Adds an array entry to the snake to make it one body part longer.
         *
         * @return {boolean}    - true if operation was successful
         *                      - false if operation was not successful
         * @author Lisa Koller
         * @private
         */
        _makeSnakeLonger(){
            //get current position of the tail = last part of snake
            let length = this._snake.length-1;
            let x = this._snake[length].x;
            let y = this._snake[length].y;

            //compare last 2 snake parts to determine the direction and add array entry
            let beforeX = this._snake[length-1].x;
            let beforeY = this._snake[length-1].y;
            //up
            if((x === beforeX) && (y < beforeY)){
                this._snake.push({x: x, y: y++});
                return true;
            }
            //down
            else if((x === beforeX) && (y > beforeY)){
                this._snake.push({x: x, y: y--});
                return true;
            }
            //left
            else if((x < beforeX) && (y === beforeY)){
                this._snake.push({x: x++, y: y});
                return true;
            }
            //right
            else if((x > beforeX) && (y === beforeY)){
                this._snake.push({x: x--, y: y});
                return true;
            }
            else {
                return false;
            }
        }

        /**
         * Sets a new random position for the item to catch and fires the newItem-event.
         *
         * @author Lisa Koller
         * @private
         */
        _setRandomItemCoords(){
            let newX;
            let newY;
            while(true){
                //randomly select a new x and y coordinate inside the borders
                newX = Math.floor(Math.random() * (this._fieldCoords["maxX"]-1));   // * 11 --> Werte 0-10
                newY = Math.floor(Math.random() * (this._fieldCoords["maxY"]-1));

                //if coordinates are either inside the snake or in the bottom left corner
                //try again to select random coordinates
                if(!this._areInTheWay(newX, newY)) break;
            }

            //set the fitting coordinates
            this._itemCoords["x"] = newX;
            this._itemCoords["y"] = newY;

            //trigger event that a new item was placed
            const e = new $.Event("newItem", {
                item: this._itemCoords
            });
            $(this).trigger(e);
        }

        /**
         * Checks if the given coords are in the snake or in the
         * bottom left corner (where the item is bad to see).
         *
         * @return {boolean}    - true if coords are in snake/corner
         *                      - false if coords are not in snake/corner
         * @author  Lisa Koller
         * @private
         */
        _areInTheWay(x, y){
            for(let i = 0; i < this._snake.length-1; i++){
                //check if in the snake
                if(this._snake[i].x === x && this._snake[i].y === y){
                    return true;
                }
                //check if in the bottom left corner
                if(x <= 5 && y >= 22){
                    return true;
                }
            }
            return false;
        }
    }

    lib.SnakeModel = SnakeModel;

}());
