
          ________    ___     __       ___       __   __    ________
         |        |  |    \  |  |     /   \     |  | /  /  |        |
         |   _____|  |     \ |  |    /     \    |  |/  /   |   _____|
         |  |_____   |  |\  \|  |   /  /_\  \   |     /    |  |_____
         |_____   |  |  | \  \  |  |         |  |    \     |   _____|
          _____|  |  |  |  \    |  |  _____  |  |     \    |  |_____
         |        |  |  |   \   |  |  |   |  |  |  |\  \   |        |
         |________|  |__|    \__|  |__|   |__|  |__| \__\  |________|


README
    Start the Game with the Button "Spiel starten".

    The Snake can be controlled with WASD or the Arrow-Keys:
    W/🡩 move upwards.
    S/🡫 move downwards.
    A/🡨  move left.
    D/🡪 move right.

    With every collected "diamond" you get 10 Points and snake gets longer and faster.
    You lose when you hit the wall or yourself.

    Please make sure to have the latest graphics driver installed.

CONTACT
    We are a team of four students that were programming and designing this game.
    We are:
    Bianaca Hollmüller
        Git-Master, Designer, Model, Tester, Render, Model-Coordinates, Collision, Move, Events

    Lisa Koller
        Model, Designer, View, SVG-Expert, CSS, Tester, Move, Directions, Events, Speed


    Lisa Prammer
        Designer, Model, CSS, Tester, Move, GameOver-Screen, Button, Timer, Score


    Maria Sams
        Designer, View, Canvas-Optimist, CSS, Tester, View-Coordinates, View-Rotation

    Additionally the authors of every method are mentioned in the comment above it (@author ...).

PROBLEMS AND SOLUTIONS
    A lesson learned for all of us has something to do with time management. At first
    we were optimistic that we could realize the project with Canvas but it turned out that
    it was not as easy as we thought it was. We should have communicated more actively to
    realize sooner that it will not work out that one person is responsible for the Canvas.
    As three of us had zero experiences with Canvas we decided to switch to SVG as we could
    all help each other using them. But as we lost a lot of time without really moving
    forward with the project there was unfortunately not enough time left to include
    a gulp-pipeline and npm-modules in the project as originally planned. On the other hand
    two of our four team members had the chance to work actively with git for the first time
    which is also a great experience that will definitely help them in the future.

    Anther - big - problem were the event handlers. At first we had problems to even
    trigger the events correctly, until we realized that we can't trigger events in the
    model if the view doesn't exist yet but should be listening to them. Once that was
    solved we struggled with restarting the game. It appeared to be triggering the events
    more than once which was a problem because for example the snake suddenly moved
    double as fast. We tried to solve it by moving the events around, deactivating them and
    such but in the end we couldn't figure out how to do this correctly and not making
    the game unplayable during the process.
    So we decided that making the restart button work in regards to letting the snake
    move in the right speed and thus making the game playable on restart is more important
    than a beautiful code. So we simply reload the page on click on the new game button
    if game over was already triggered. This has one downside though: You have to click twice
    on the button because on reload (of course) the page is displayed as if you just
    landed on the page for the first time which means that there is an empty playfield
    and you can start the game by clicking the button. But in this case you already have
    clicked that button just a second ago. So this is still a bug that needs to be fixed
    but sadly we do not have the time work further on this project (although we really
    loved it by the way ❤).

    At the start of the game, when the snake is slower, there is a bug that appears when
    the WASD or Arrow-Keys are pressed too fast and the snake should make an U-Turn -
    instead it bites itself which results in a game over.
    We couldn't really figure out why it does that. But it is quite unlikely that a player
    will make that move right at the beginning of the game anyway.

GIT
    Link to the repository: https://bitbucket.org/biancagit/advancedgaming.git

    Instructions for using git:
    git add .
    git commit -m "random"
    git push

    git pull

INDIVIDUAL COMMENT
    Bianca Höllmüller
        - I was responsible for collision detection and for troubleshooting. It was a
          challenge to debug the code, which the others wrote. After the snake was able
          to move, it was much easier for me to test everything we already implemented.
          I really enjoyed to work with the others of the team, everybody did awesome
          work!

    Lisa Koller
        - The greatest hurdle for me was the keeping track of all the events and which
          one was fired where and because of what. This was especially tricky because
          we were working in a team of four people instead of everyone for herself like
          at most other programming tasks. Sometimes you had to re-read the code over and
          over again to figure out what was changed or moved around by the others.
          This got a lot better in the second half of the project as we cooperated more
          closely, added comments to the methods and told everyone when we changed
          something substantial.
        - Another big hurdle was the interval - in the beginning it just didn't do what
          I expected it to do. It called itself far too often and not just every
          second as I told it to. But with help of the others and especially
          Bianca and her debugging skills we managed to make it work correctly.

    Lisa Prammer
        - One hurdle for me was that we were working in a team of four and obviously
          everyone has a different style. So it took some time to get used to the
          different coding styles.

    Maria Sams
        - At the beginning we planned to create an Jump&Run game with JavaScript and wanted
          to try the game engine phaser.io but we hadn't so much time and a lot other tasks
          to do. So we decided to make the game SNAKE with Canvas but it was also the
          problem, that the time was to short and I was the only one that is familiar with
          Canvas. So nobody could help me with problems and the problems came with the
          animation because the snake didn't move and we couldn't really say what the problem
          was. So we decided to build our SNAKE with SVG.
        - At the beginning I didn't really understand what the others planned. So I read the code
          from the others first. That helped me to understand how the model worked to create
          the view at the beginning.
        - At the end more and more bugs appeared and it was a challenge to find the problems.
          For example right velocity or event handling problems.