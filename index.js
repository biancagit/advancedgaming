$(function () {
    //create classes needed for the game
   let model = new lib.SnakeModel();
   let view = new lib.SnakeView(model);

   //start the game on click on the button
   $('.button').on('click', function () {
       model.start();
       $('.gameover').remove();
   });
});